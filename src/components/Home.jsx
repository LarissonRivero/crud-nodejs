import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
  return (
    <div>
      <h1>Home Page</h1>
      <p>Bienvenido a la página de inicio.</p>
      <Link to="/form">
        <button>Ir a Registrarse</button>
      </Link>
    </div>
  );
}

export default Home;