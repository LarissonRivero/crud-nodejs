/**
 @param {string} name - Nombre del Usuario
 @param {string} lname - Apellido del Usuario
 @param {string} email - Correo Electronico del Usuario
 @param {string} password - Contraseña del Usuario
 @param register - Funcion de React Hook Form que nos permite acceder a los metodos de registro de los inputs
 @param handleSubmit - Funcion de React Hook Form que nos permite acceder a los metodos de envio de los datos del formulario
 @param formState - Funcion de React Hook Form que nos permite acceder a los metodos de validacion de los inputs, en este caso nos permite acceder a los errores que genera el formulario
  @param watch - Funcion de React Hook Form que nos permite comparar los valores de los inputs de contraseña y correo electronico
  @param reset - Funcion de React Hook Form que nos permite resetear los valores de los inputs del formulario
*/

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useForm } from 'react-hook-form';
import Alert from 'react-bootstrap/Alert';
import { useState } from 'react';

function FormExample() {
  const { register, handleSubmit, formState : {errors}, watch, reset} = useForm();
  console.log(errors);

  const [show, setShow] = useState(false);

  const onSubmit = handleSubmit((data) => {console.log(data)
    reset();
    setShow(true);
    setTimeout(() => {
    setShow(false);
    }, 3000);
  });

  return (
    <div className="Container">
      <h1 className="text-center p-2">Formulario de Registro</h1>
        <div className="row justify-content-center align-items-center">
        <div className="col-md-6">
          <form onSubmit={onSubmit}>
            <div className="App-header m-1 p-4 bg-light rounded text-center border border-5">
              <div className="row mb-6">
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="fname">Nombre</Form.Label>
                  <Form.Control
                    type="text" 
                    {...register("name", {
                      required: true,
                      minLength: 3,
                    })}
                    placeholder="Ingrese su Nombre"
                  />
                  {
                  errors.name?.type === "required" && 
                  <span className="text-danger d-block">El Nombre es Requerido</span>
                  }
                  {
                  errors.name?.type === "minLength" && <h6 className="text-danger d-block">Debe Contener mas de 3 Caracteres</h6>
                  }
                </div>
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="name">Apellido</Form.Label>
                  <Form.Control
                    type="text" 
                    {...register("lname", {
                      required: true,
                      minLength: 3,
                    })}
                    placeholder="Ingrese su Apellido"
                  />
                  {
                  errors.lname?.type === "required" && 
                  <span className="text-danger d-block">El Apellido es Requerido</span>
                  }
                  {
                  errors.lname?.type === "minLength" && <span className="text-danger d-block">Debe Contener mas de 3 Caracteres</span>
                  }
                </div>
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="name">Correo Electronico</Form.Label>
                  <Form.Control
                    type="email" 
                    {...register("email", {
                      required: true,
                      minLength: 2,
                      pattern: { 
                      value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/, 
                      message: "Ingrese un Correo Valido" 
                    }
                  })}
                    placeholder="Ingrese un correo Electronico"
                  />
                   {
                  errors.email?.type === "required" && 
                  <span className="text-danger d-block">El Correo es Requerido</span>
                  }
                  {
                  errors.email && 
                  <span className="text-danger d-block">{errors.email.message}</span>
                  }
                </div>
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="name">Confirmar Correo</Form.Label>
                  <Form.Control
                    type="email" 
                    {...register("cemail", {
                      required: {
                        value: true,
                        message: "Confirme su Correo Electronico"
                      },
                      validate: (value) => value === watch('email') || "Los Correos no Coinciden"
                    })}
                    placeholder="Confirme su Correo Electronico"
                  />
                  {errors.cemail && <span className="text-danger d-block">{errors.cemail.message}</span>}
                </div>
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="name">Contraseña</Form.Label>
                  <Form.Control
                    type="password" 
                    {...register("password", {
                      required: true,
                      minLength: 6,
                    })}
                    placeholder="Ingrese una Contraseña"
                  />
                  {
                  errors.password?.type === "required" && 
                  <span className="text-danger d-block">La Contraseña es Requerida</span>
                  }
                  {
                  errors.password?.type === "minLength" && <span className="text-danger d-block ">La Contraseña debe contener al menos 6 caracteres</span>
                  }
                </div>
                <div className="col-md-6">
                  <Form.Label className='fs-6 fw-bold' htmlFor="name">Confirmar Contraseña</Form.Label>
                  <Form.Control
                    type="password" 
                    {...register("cpassword", {
                      required: {
                      value: true,
                      message: "La Confirmacion de la Contraseña es Requerida"
                    },
                    validate: (value) => value === watch('password') || "Las Contraseñas no Coinciden"
                    })}
                    placeholder="Confirme su Contraseña"
                  />
                  {errors.cpassword && <span className="text-danger d-block">{errors.cpassword.message}</span>}
                </div>
                  <br />
              <div className="col-md-12">
                <Button type="submit" onClick={() => setShow(true) }className='fw-bold col-md-4 mt-4 p-2' variant="success">Registrarse</Button>
              </div>
            </div>
            </div>
            {show && (
            <Alert variant="primary" onClose={() => setShow(false)} dismissible>
              Registro Exitoso!!!
            </Alert>
          )}
          </form>
          
          </div>
        </div>
    </div>
  );
}

export default FormExample;